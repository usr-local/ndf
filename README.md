# ndf
Neat Duplicate Finder

Shell based tool for identifying duplicate files in a given directory.
I mostly use it for identifying duplicates in my photo collection; others might
find it useful for other things.
It's true it might be slow but using SHA256 hashing assures me I don't lose any
useful bit of data in my collection.

Run `ndf.sh` without any arguments for a short usage description.

# Dependencies

A `sha256`  program.
