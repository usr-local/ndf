#!/bin/sh
# TODO: Total amount of space saved if cleaned up
set -e

SAFEFILES="$(mktemp)"
export PATH=/usr/bin:/bin:/usr/local/bin

usage(){
    cat <<EOF
Usage:
  $(basename "$0") [ -s ] dir_to_check

ARGS
  -s: Print a summary of saved disk space if duplicates are deleted

ENV VARS
  FSIZE: Max file size in bytes to check for; defaults to 1024000 (~10MB)

BUGS
  File sizes under a megabyte show up as 0
EOF
}

# Make sure stuff gets cleaned
trap 'rm ${SAFEFILES}' TERM EXIT

if [ $# -eq 0 ]; then
  usage
  exit 1
fi

search_dir(){
    # Globbing and word splitting are desired here
    # I want to be able to pass arguments to the find & xargs commands
    # Couldn't find a way to do it while quoting the variables
    # shellcheck disable=SC2086
    find "${1}" -type f ${FIND_ARGS} -print0 2>/dev/null \
         | xargs -0 ${SHA256}
}

log_error(){
    >&2 printf "%s\\n" "$*"
}

log_info(){
    printf "%s\\n" "$*"
}

print_safe_files(){
    awk '/'"${1}"'/ { $1=""; { gsub(/^ /, "", $0); }; print $0 }' "${SAFEFILES}"
}

print_file_name(){
    awk '{ x=$1; $1=""; { gsub(/^ /, "", $0); }; print $0 }'
}

print_unique_hashes(){
    cut -d" " -f1 "${SAFEFILES}" | uniq -d
}

calculate_summary(){
    times_saved=$(($(grep -c "${1}" "${SAFEFILES}") - 1))
    case $(uname) in
        OpenBSD)
            size=$(stat -f "%z" "$(grep -m 1 "${1}" "${SAFEFILES}" | print_file_name)")
            ;;
        Linux)
            size=$(stat --format "%s" "$(grep -m 1 "${1}" "${SAFEFILES}" | print_file_name)")
            ;;
    esac
    saving=$((times_saved * size))
    if [ ${times_saved} -eq 1 ]; then
        log_info "Deleting the additional copy of hash ${1} saves:"
    elif [ ${times_saved} -gt 1 ]; then
        echo "Deleting the ${times_saved} additional copies of hash ${1} saves:"
    fi
    log_info "${saving} bytes"
    log_info "$((saving / 1024)) kilobytes"
    log_info "$((saving / 1024 / 1024)) megabytes"
}

# Find out which platform is running the script
case $(uname) in
    OpenBSD)
        NEEDEDPROGS="sha256"
        SHA256="sha256 -r"
        FIND_ARGS="-size -${FSIZE:-1024000}c"
        ;;
    Linux)
        NEEDEDPROGS="sha256sum"
        SHA256="sha256sum"
        FIND_ARGS="-size -${FSIZE:-1024000}c"
        ;;
    *)
        log_error "Platform not supported"
        ;;
esac

# Make sure all required programs are available
for i in ${NEEDEDPROGS}; do
    command -v "${i}" > /dev/null 2>&1 || { log_error \
                                                "Cannot find ${i}.";
                                            log_error "Exiting";
                                            exit 1; }
done

# Parse arguments
while getopts s arg; do
    case ${arg} in
        s)
            SUMMARY=true
            ;;
        *) usage
           exit 1
           ;;
    esac
done
shift $((OPTIND - 1))

# Expect a directory as first non-optional argument
SOURCEDIR="$1"

# Make sure given directory exists
[ -d "${SOURCEDIR}" ] || { log_error \
                        "${SOURCEDIR} is not a directory";
                    log_error "Exiting";
                    exit 1; }

# Find relevant files, compute hashes and store in temporary file
search_dir "${SOURCEDIR}" | sort > "${SAFEFILES}"

if [ "${SUMMARY}" ]; then
        for i in $(awk '{ print $1 }' "${SAFEFILES}" | uniq -d); do
            calculate_summary "${i}"
        done
else
    # Print matched hashes
    for i in $(print_unique_hashes); do
        log_info "Hash ${i} matches:"
        print_safe_files "${i}"
    done
fi
